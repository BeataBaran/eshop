import FSMCart._
import FSMCheckout.CheckoutStarted
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

class FSMCartTest extends TestKit(ActorSystem("EShop")) with WordSpecLike with BeforeAndAfterAll with ImplicitSender{
  import akka.testkit.TestFSMRef

  var shop: TestActorRef[EShop] = _
  var customer: TestFSMRef[Customer.State, Customer.Data, Customer] = _
  var cart: TestFSMRef[State, Data, FSMCart] = _

  override def afterAll(): Unit = {
    system.terminate()
  }

  override def beforeAll(): Unit = {
    shop = TestActorRef(new EShop(system))
    customer = TestFSMRef(new Customer(system, shop))
    cart = TestFSMRef(new FSMCart(system, customer), "cart1")
  }


  "A Cart" must {
    "start in Empty state" in {
      assert(cart.stateName == Empty)
      assert(cart.stateData == Uninitialized)
      assert(!cart.underlyingActor.timers.isTimerActive(CartTimer))
      assert(cart.underlyingActor.itemCount == 0)
    }

    "after adding items change state for NonEmpty" in {
      assert(cart.stateName == Empty)
      assert(cart.underlyingActor.itemCount == 0)

      cart ! ItemAdded(2)

      assert(cart.stateName == NonEmpty)
      assert(cart.stateData == Uninitialized)
      assert(cart.underlyingActor.itemCount == 2)
      assert(cart.underlyingActor.timers.isTimerActive(CartTimer))
    }

    "return actorRef of Checkout actor" in {
      cart ! CheckoutStarted
      expectMsgPF() {
        case Customer.CheckoutStarted(_) => ()
      }
    }
  }


}
