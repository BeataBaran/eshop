
import FSMCart.CheckoutClosed
import FSMCheckout.{DeliveryMethodSelected, PaymentReceived, PaymentSelected}
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestActorRef, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}


class CartCheckoutTest extends TestKit(ActorSystem("EShop_CartCheckout")) with WordSpecLike with BeforeAndAfterAll with ImplicitSender{
  import akka.testkit.TestFSMRef
  var shop: TestActorRef[EShop] = _
  var customer: TestFSMRef[Customer.State, Customer.Data, Customer] = _
  var checkout: TestFSMRef[FSMCheckout.State, FSMCheckout.Data, FSMCheckout] = _
  var cart: ActorRef = _

  override def afterAll(): Unit = {
    system.terminate()
  }

  override def beforeAll(): Unit = {
    shop = TestActorRef(new EShop(system))
    customer = TestFSMRef(new Customer(system, shop))
  }

  "A Cart as parent" should {
    "test Checkout responses" in {
      val parent = TestProbe()
      val child = parent.childActorOf(Props(new FSMCheckout(system, customer, parent.testActor)))
      child ! DeliveryMethodSelected
      child ! PaymentSelected
      child ! PaymentReceived
      parent.expectMsg(CheckoutClosed)
    }
  }

}

