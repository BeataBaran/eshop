import java.util.concurrent.TimeUnit

import Customer.CartEmpty
import FSMCart.Data
import FSMCart.State
import FSMCheckout.CheckoutStarted
import akka.actor.{ActorLogging, ActorRef, ActorSystem, FSM, Props, Timers}

import scala.concurrent.duration.FiniteDuration

object FSMCart{
  def props(system: ActorSystem, customer: ActorRef): Props = Props(new FSMCart(system, customer))
  sealed trait State
  case object Empty extends State
  case object NonEmpty extends State
  case object InCheckout extends State
  case class ItemAdded(amount: BigInt) {
    require(amount > 0)
  }
  case class ItemRemoved(amount: BigInt) {
    require(amount > 0)
  }
  case object CheckoutCancelled
  case object CheckoutClosed
  case object CartTimerExpired
  case object CartTimer
  sealed trait Data
  case object Uninitialized extends Data
}

class FSMCart(system: ActorSystem, customer: ActorRef) extends FSM[State, Data] with ActorLogging with Timers{
  import FSMCart._
  var itemCount  = BigInt(0)
  startWith(Empty, Uninitialized)

  when(Empty) {
    case Event(ItemAdded(amount), _)=>
      log.debug("ItemAdded " + amount + " received")
      itemCount += amount
      goto(NonEmpty)
  }

  when(NonEmpty) {
    case Event(ItemAdded(amount), _) =>
      log.debug("ItemAdded " + amount + " received")
      itemCount += amount
      resetTimer()
      stay
    case Event(ItemRemoved(amount), _) if amount == itemCount =>
      log.debug("ItemRemoved " + amount + " received, itemCount: " + itemCount)
      itemCount = 0
      goto(Empty)
    case Event(ItemRemoved(amount), _) if amount < itemCount =>
      log.debug("ItemRemoved " + amount + " received, itemCount: " + itemCount)
      itemCount -= amount
      resetTimer()
      stay
    case Event(CheckoutStarted, _) =>
      log.debug("CheckoutStarted received")
      val checkout : ActorRef = system.actorOf(FSMCheckout.props(system, customer, self), "checkout")
      sender ! Customer.CheckoutStarted(checkout)
      goto(InCheckout)
    case Event(CartTimerExpired, _) =>
      log.debug("CartTimerExpired received")
      itemCount = 0
      goto(Empty)
  }

  when(InCheckout) {
    case Event(CheckoutCancelled, _) =>
      log.debug("CheckoutCancelled received")
      goto(NonEmpty)
    case Event(CheckoutClosed, _) =>
      log.debug("CheckoutClosed received")
      customer ! CartEmpty
      goto(Empty)
  }

  onTransition {
    case Empty -> NonEmpty =>
      timers.startSingleTimer(CartTimer, CartTimerExpired, FiniteDuration(5, TimeUnit.MINUTES))
    case NonEmpty -> InCheckout =>
      timers.cancel(CartTimer)
    case NonEmpty -> Empty =>
      timers.cancel(CartTimer)
    case InCheckout -> NonEmpty =>
      timers.startSingleTimer(CartTimer, CartTimerExpired, FiniteDuration(5, TimeUnit.MINUTES))
  }

  def resetTimer(): Unit = {
    timers.cancel(CartTimer)
    timers.startSingleTimer(CartTimer, CartTimerExpired, FiniteDuration(5, TimeUnit.MINUTES))
  }

  def getItemCount: BigInt = {
    itemCount
  }
}
