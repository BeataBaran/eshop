import Customer.DoPayment
import FSMCheckout.PaymentReceived
import PaymentService.{Data, State}
import akka.actor.{ActorLogging, ActorRef, FSM, Props}

object PaymentService{
  def props(customer: ActorRef, checkout: ActorRef): Props = Props(new PaymentService(customer, checkout))
  sealed trait State
  case object Primary extends State
  sealed trait Data
  case object Uninitialized extends Data
  case object PaymentConfirmed
}

class PaymentService(customer: ActorRef, checkout: ActorRef) extends FSM[State, Data] with ActorLogging{
  import PaymentService._
  startWith(Primary, Uninitialized)

  when(Primary) {
    case Event(DoPayment, _) =>
      customer ! PaymentConfirmed
      checkout ! PaymentReceived
      stay
  }
}
