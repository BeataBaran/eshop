import Customer._
import FSMCheckout.{DeliveryMethodSelected, PaymentSelected}
import akka.actor.{Actor, ActorRef, ActorSystem, Props}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object EShop extends App {
  def props(system: ActorSystem): Props = Props(new EShop(system))
  case object Init

  val system: ActorSystem = ActorSystem("EShop")
  val eShop = system.actorOf(EShop.props(system), "eshop")
  eShop ! Init
  Await.result(system.whenTerminated, Duration.Inf)
}

class EShop(system: ActorSystem) extends Actor {
  import EShop._
  var customer: ActorRef = _

  def receive(): Receive = {
    case Init =>
      customer = system.actorOf(Customer.props(system, self), "customer")
      customer ! AddItem(3)
      customer ! RemoveItem(1)
      customer ! StartCheckout

    case CheckoutStarted =>
      customer ! DeliveryMethodSelected
      customer ! PaymentSelected

    case PaymentServiceStarted =>
      customer ! DoPayment
  }
}
