
import java.util.concurrent.TimeUnit

import Customer.PaymentServiceStarted
import FSMCart.CheckoutClosed
import FSMCheckout.{Data, State}
import akka.actor.{ActorLogging, ActorRef, ActorSystem, FSM, Props, Timers}

import scala.concurrent.duration.FiniteDuration

object FSMCheckout {
  def props(system: ActorSystem, customer: ActorRef, cart: ActorRef): Props = Props(new FSMCheckout(system, customer, cart))
  sealed trait State
  case object SelectingDelivery extends State
  case object SelectingPaymentMethod extends State
  case object ProcessingPayment extends State
  case object Cancelled extends State
  case object Closed extends State

  case class CheckoutStarted(itemCount : BigInt) {
    require(itemCount > 0)
  }
  case object DeliveryMethodSelected
  case object PaymentSelected
  case object PaymentReceived
  case object Cancel

  case object CheckoutTimer
  case object CheckoutTimerExpired
  case object PaymentTimer
  case object PaymentTimerExpired
  sealed trait Data
  case object Uninitialized extends Data
}

class FSMCheckout(system: ActorSystem, customer: ActorRef, cart: ActorRef) extends FSM[State, Data] with ActorLogging with Timers{
  import FSMCheckout._

  startWith(SelectingDelivery, Uninitialized)
  timers.startSingleTimer(CheckoutTimer, CheckoutTimerExpired, FiniteDuration(5, TimeUnit.MINUTES))

  when(SelectingDelivery) {
    case Event(DeliveryMethodSelected, _) =>
      log.debug("DeliveryMethodSelected received")
      goto(SelectingPaymentMethod)
    case Event(Cancel, _) =>
      log.debug("Cancel received")
      goto(Cancelled)
    case Event(CheckoutTimerExpired, _) =>
      log.debug("CheckoutTimerExpired received")
      goto(Cancelled)
  }

  when(SelectingPaymentMethod) {
    case Event(PaymentSelected, _) =>
      log.debug("PaymentSelected received")
      val paymentService : ActorRef = system.actorOf(PaymentService.props(customer, self), "paymentService")
      customer ! PaymentServiceStarted(paymentService)
      goto(ProcessingPayment)
    case Event(Cancel, _) =>
      log.debug("Cancel received")
      goto(Cancelled)
    case Event(CheckoutTimerExpired, _) =>
      log.debug("CheckoutTimerExpired received")
      goto(Cancelled)
  }

  when(ProcessingPayment) {
    case Event(PaymentReceived, _) =>
      log.debug("PaymentReceived received")
      customer ! CheckoutClosed
      cart ! CheckoutClosed
      goto(Closed)
    case Event(Cancel, _) =>
      log.debug("Cancel received")
      goto(Cancelled)
    case Event(PaymentTimerExpired, _) =>
      log.debug("PaymentTimerExpired received")
      goto(Cancelled)
  }

  when(Cancelled) {
    case Event(_, _) =>
      stay()
  }

  when(Closed) {
    case Event(_, _) =>
      stay()
  }


  onTransition {
    case SelectingPaymentMethod -> ProcessingPayment =>
      timers.cancel(CheckoutTimer)
      timers.startSingleTimer(PaymentTimer, PaymentTimerExpired, FiniteDuration(5, TimeUnit.MINUTES))
    case _ -> Cancelled =>
      context.stop(self)
    case _ -> Closed =>
      context.stop(self)
  }
}
