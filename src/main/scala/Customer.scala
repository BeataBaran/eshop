import Customer.{Data, State}
import FSMCart.CheckoutClosed
import FSMCheckout.{DeliveryMethodSelected, PaymentSelected}
import PaymentService.PaymentConfirmed
import akka.actor.{ActorLogging, ActorRef, ActorSystem, FSM, Props}


object Customer{
  def props(system: ActorSystem, shop: ActorRef): Props = Props(new Customer(system, shop))
  sealed trait State
  case object SelectingProducts extends State
  case object Checkout extends State
  case object Payment extends State

  case object StartCheckout
  case object CheckoutStarted
  case object PaymentServiceStarted
  case object DoPayment
  case class PaymentServiceStarted(paymentService: ActorRef)
  case class AddItem(amount: BigInt) {
    require(amount > 0)
  }
  case class RemoveItem(amount: BigInt) {
    require(amount > 0)
  }
  case class CheckoutStarted(checkoutRef: ActorRef)
  case object CartEmpty

  sealed trait Data
  case object Uninitialized extends Data
}

class Customer(system: ActorSystem, shop: ActorRef) extends FSM[State, Data] with ActorLogging {

  import Customer._

  var itemCount = BigInt(0)
  val cart: ActorRef = system.actorOf(FSMCart.props(system, self), "cart")
  var checkout: ActorRef = _
  var paymentService: ActorRef = _
  startWith(SelectingProducts, Uninitialized)


  when(SelectingProducts) {
    case Event(AddItem(amount), _) =>
      log.debug(amount + " item(s) added")
      cart ! FSMCart.ItemAdded(amount)
      stay
    case Event(RemoveItem(amount), _) =>
      log.debug(amount + " item(s) removed")
      cart ! FSMCart.ItemRemoved(amount)
      stay
    case Event(StartCheckout, _) =>
      log.debug("Checkout started")
      cart ! FSMCheckout.CheckoutStarted
      stay
    case Event(CheckoutStarted(checkoutRef), _) =>
      checkout = checkoutRef
      log.debug("Checkout created")
      goto(Checkout)
  }

  when(Checkout) {
    case Event(DeliveryMethodSelected, _) =>
      log.debug("Delivery method selected")
      checkout ! DeliveryMethodSelected
      stay
    case Event(PaymentSelected, _) =>
      log.debug("Payment method selected")
      checkout ! PaymentSelected
      stay
    case Event(PaymentServiceStarted(paymentServiceRef), _) =>
      log.debug("Payment service created")
      paymentService = paymentServiceRef
      goto(Payment)
  }

  when(Payment) {
    case Event(DoPayment, _) =>
      log.debug("Payment received")
      paymentService ! DoPayment
      stay
    case Event(PaymentConfirmed, _) =>
      log.debug("Payment confirmed")
      stay
    case Event(CheckoutClosed, _) =>
      log.debug("Checkout closed")
      stay
    case Event(CartEmpty, _) =>
      log.debug("Transaction finished")
      goto(SelectingProducts)
  }

  onTransition {
    case SelectingProducts -> Checkout =>
      shop ! CheckoutStarted
    case Checkout -> Payment =>
      shop ! PaymentServiceStarted
  }
}