/*
package unused

import akka.actor.{Actor, ActorRef, ActorSystem, Props}

object Coordinator {
  def props(system: ActorSystem): Props = Props(new Coordinator(system))
  case object Init
  case object InitFSM
  case class StartCheckout(checkout: ActorRef)
  case class StartFSMCheckout(checkout: ActorRef)
}

class Coordinator(system: ActorSystem) extends Actor{
  import Coordinator._

  def receive: Receive = {
    case Init =>
      val cart1 = system.actorOf(Cart.props(system), "cart_1")
      val cart2 = system.actorOf(Cart.props(system), "cart_2")

      cart1 ! Cart.ItemAdded(2) //add to empty
      cart1 ! Cart.ItemAdded(1) //add to nonempty
      cart1 ! Cart.ItemRemoved(1) //remove and stay in nonempty
      cart1 ! Cart.ItemRemoved(2) //remove go to empty
      cart1 ! Cart.ItemRemoved(1) //try to remove from empty
      cart1 ! Cart.ItemAdded(5)
      cart1 ! Checkout.CheckoutStarted

      cart2 ! Cart.ItemAdded(1)


    case StartCheckout(checkout) =>
      checkout ! Checkout.DeliveryMethodSelected
      checkout ! Checkout.PaymentSelected
      checkout ! Checkout.PaymentReceived

    case Cart.Done =>
      System.out.println("Done")
    case Cart.Failed =>
      System.out.println("Failed")

    case InitFSM =>
  /*    val FSMCart1 = system.actorOf(FSMCart.props(system), "fsm_cart_1")
      val FSMCart2 = system.actorOf(FSMCart.props(system), "fsm_cart_2")


      FSMCart1 ! FSMCart.ItemAdded(2) //add to empty
      FSMCart1 ! FSMCart.ItemAdded(1) //add to nonempty
      FSMCart1 ! FSMCart.ItemRemoved(1) //remove and stay in nonempty
      FSMCart1 ! FSMCart.ItemRemoved(2) //remove go to empty
      FSMCart1 ! FSMCart.ItemRemoved(1) //try to remove from empty
      FSMCart1 ! FSMCart.ItemAdded(5)
      FSMCart1 ! FSMCheckout.CheckoutStarted

      FSMCart2 ! FSMCart.ItemAdded(1)
*/
    case StartFSMCheckout(checkout) =>
      checkout ! FSMCheckout.DeliveryMethodSelected
      checkout ! FSMCheckout.PaymentSelected
      checkout ! FSMCheckout.PaymentReceived

  }
}
*/
