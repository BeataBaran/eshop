/*
package unused

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef, ActorSystem, Props, Timers}
import akka.event.LoggingReceive

import scala.concurrent.duration.FiniteDuration


object Cart {
  def props(system: ActorSystem): Props = Props(new Cart(system))
  case class ItemAdded(amount: BigInt) {
    require(amount > 0)
  }
  case class ItemRemoved(amount: BigInt) {
    require(amount > 0)
  }

  case object Done
  case object Failed
  case object CartTimerExpired
  case object CartTimer
}

class Cart(system: ActorSystem) extends Actor with Timers{
  import Cart._

  var itemCount  = BigInt(0)


  def empty(): Receive = LoggingReceive {
    case ItemAdded(amount) =>
      itemCount += amount
      sender ! Done
      timers.startSingleTimer(CartTimer, CartTimerExpired, FiniteDuration(5, TimeUnit.MINUTES))
      context become nonEmpty()
    case _ =>
      sender ! Failed
  }

  def nonEmpty(): Receive = LoggingReceive {
    case ItemAdded(amount) =>
      itemCount += amount
      resetTimer()
      sender ! Done
    case ItemRemoved(amount) if amount == itemCount =>
      itemCount -= amount
      sender ! Done
      timers.cancel(CartTimer)
      context become empty
    case ItemRemoved(amount) if amount < itemCount =>
      itemCount -= amount
      resetTimer()
      sender ! Done
    case CheckoutStarted =>
      timers.cancel(CartTimer)
      val checkout : ActorRef = system.actorOf(Props[Checkout], "checkout")
      sender ! Coordinator.StartCheckout(checkout)
      context become inCheckout()
    case CartTimerExpired =>
      itemCount = 0
      context become empty()
    case _ =>
      sender ! Failed
  }

  def inCheckout(): Receive = LoggingReceive {
    case CheckoutCancelled =>
      context become nonEmpty()
    case CheckoutClosed =>
      itemCount = 0
      context become empty()
  }

  def resetTimer(): Unit = {
    timers.cancel(CartTimer)
    timers.startSingleTimer(CartTimer, CartTimerExpired, FiniteDuration(5, TimeUnit.MINUTES))
  }

  def receive: Receive = empty()
}
*/
