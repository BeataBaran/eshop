/*
package unused

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, Timers}
import akka.event.LoggingReceive

import scala.concurrent.duration.FiniteDuration

object Checkout {
  case class CheckoutStarted(itemCount: BigInt) {
    require(itemCount > 0)

  }
  case object CheckoutCancelled
  case object CheckoutClosed

  case object CheckoutTimer
  case object CheckoutTimerExpired
  case object PaymentTimer
  case object PaymentTimerExpired

  case object PaymentReceived
  case object PaymentSelected
  case object DeliveryMethodSelected
  case object Done

}

class Checkout extends Actor with Timers{
  import Checkout._
  timers.startSingleTimer(CheckoutTimer, CheckoutTimerExpired, FiniteDuration(5, TimeUnit.MINUTES))

  def selectingDelivery() : Receive = LoggingReceive{
    case DeliveryMethodSelected =>
      context become selectingPaymentMethod
    case CheckoutTimerExpired =>
      context become cancelled()
      self ! Done
    case CheckoutCancelled =>
      context become cancelled()
      self ! Done
  }

  def selectingPaymentMethod() : Receive = LoggingReceive{
    case PaymentSelected =>
      timers.startSingleTimer(PaymentTimer, PaymentTimerExpired, FiniteDuration(5, TimeUnit.MINUTES))
      context become processingPayment()
    case CheckoutTimerExpired =>
      context become cancelled()
      self ! Done
    case CheckoutCancelled =>
      context become cancelled()
      self ! Done

  }

  def processingPayment() : Receive = LoggingReceive{
    case PaymentReceived =>
      context become closed()
      self ! Done
    case PaymentTimerExpired =>
      context become cancelled()
      self ! Done
    case CheckoutCancelled =>
      context become cancelled()
      self ! Done
  }

  def cancelled(): Receive = LoggingReceive{
    case Done =>
      context.stop(self)
  }

  def closed() : Receive = LoggingReceive{
    case Done =>
      context.stop(self)
  }

  def receive: Receive = selectingDelivery()
}
*/
